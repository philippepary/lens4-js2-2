// We create a global scoped object
// We could also have written var studentsJSON = {};
var studentsJSON = new Object();

// We're retriving data from the internet with fetch
// the first then tells that the data fetched is JSON
// the second then is the code we actually want to execute
// the catch is our error handling code
fetch(`http://api-students.popschool-lens.fr/students.json`)
    .then(response => response.json())
    .then(myJSON => {
        console.log("1");
        console.log(myJSON);
        studentsJSON = myJSON;
        
        // We're having a look at our variable
        console.log("2");
        console.log(studentsJSON);
        
        // We display each student
        studentsJSON.students.forEach(student => {
            // console.log(student.firstname);
            // Create the bootstrap card container
            var studentCard = document.createElement("div");
            studentCard.className = "card";
            studentCard.style.width = "18rem";
            document.querySelector("main").appendChild(studentCard);
            // Create the image of the card
            var studentCardImg = document.createElement("img");
            studentCardImg.src = "https://via.placeholder.com/180x50";
            studentCardImg.alt = "Card image cap";
            studentCardImg.className = "card-img-top";
            // Attach the image of the card to the card div
            studentCard.appendChild(studentCardImg);
            // Create the card-body div
            var studentCardBody = document.createElement("div");
            studentCardBody.className = "card-body";
            // Attach the card-body div to the card div
            studentCard.appendChild(studentCardBody);
            // Attach the h5 of the card
            var studentCardBodyH5 = document.createElement("h5");
            studentCardBodyH5.className = "card-title";
            studentCardBodyH5.innerHTML = `${student.firstname} ${student.lastname}`;
            // Attach the H5 to the card div
            studentCardBody.appendChild(studentCardBodyH5);
            // Here you have to create the card-text p. Demerden sie sich
            // Create the card button
            var studentCardBodyBtn = document.createElement(`a`);
            studentCardBodyBtn.href = `#`;
            studentCardBodyBtn.className = `btn btn-primary`;
            studentCardBodyBtn.innerHTML = `Modifier`;
            // Attach the button to the card div
            studentCardBody.appendChild(studentCardBodyBtn)
        })
    })
    .catch(error => { console.log(error) })